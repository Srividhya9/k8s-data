# k8s-data
The project involves deploying an HTML application onto Kubernetes using GitLab CI/CD. The project structure includes three branches: Dev, Prod, and Main. The Dev branch is responsible for deployments to the dev namespace, while the Prod branch handles deployments to the Prod namespace. Below are the outlined requirements and the overall structure of the project.

**Prerequisites:**

- Docker installed on your local computer.
- Minikube set up on your local machine.
- GitLab account.

**Steps to Setup Project:**

**Set Up Docker:**
Install Docker on your local computer if you haven't already. You can download it from the official Docker website and follow the installation instructions for your operating system.

**Setting Up Minikube:**
Install Minikube on your local machine. You can find installation instructions on the Minikube GitHub repository or the official Minikube documentation.

**Create Empty Project on GitLab:**
Log in to your GitLab account and create a new empty project where you will host your code.

**Create Agent Configuration:**
Set up a configuration file for your agent to connect to the Kubernetes cluster created by Minikube. This file typically includes cluster credentials and other configuration details.

**Create HTML File:**
Create a simple HTML file with the content "Hello World" and save it in your project directory.

**Write Dockerfile:**
Create a Dockerfile in your project directory. This file will contain instructions for building your Docker image.

**Configure Dockerfile:**
Configure the Dockerfile to copy your HTML file into the Nginx server's document root.

**Write .gitlab-ci.yaml:**
Create a .gitlab-ci.yml file in your project directory. This file defines the CI/CD pipeline for your project. 

**Commit and Push Changes:**
Commit all your changes to your local Git repository and push them to the GitLab repository you created earlier.

**CI/CD Pipeline Automation:**
GitLab CI/CD will automatically trigger whenever you push changes to your repository. 
**build stage** it will build your Docker image and push it to the GitLab Container Registry.
**Deployment stage** it pulls latest image to the Kubernetes.
**Rollback stage** it apply rollback to the application, it rolls to latest sucessful deployment.

**Rollback strategy**

We have incorporated both manual and automatic rollback strategies into the GitLab pipeline.
- As shown in the application pipeline, In case of deployment failure, manual rollback can be initiated by clicking the rollback button in the pipeline. 
- Additionally, GitLab offers an 'Automatic deployment rollback' feature, which automatically initiates rollback procedures upon deployment failure.

When deploying applications according to our chosen deployment strategy (such as Rolling-Update, Blue-Green, Canary, etc.), we configure our rollback strategy accordingly.








